CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Waypoints is a small jQuery plugin that makes it easy to execute a function
whenever you scroll to an element.

Waypoints makes a solid base for modern UI patterns that depend on a user’s
scroll position on the page

For a full description of the module, visit the
[project page](https://www.drupal.org/project/waypoints).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/waypoints).


REQUIREMENTS
------------

Use composer to install module,
then it will install levmyshkin/waypoints automatically.

INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. For further
   information, see
   [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

2. If you install modules manually (without composer),
   then you will need to put waypoints library from here:
   `https://github.com/levmyshkin/waypoints`

3. So jquery.waypoints.min.js file will be placed in:
   `/libraries/waypoints/lib/jquery.waypoints.min.js`


CONFIGURATION
-------------

Open the module configuration form:
`/admin/config/user-interface/waypoints`

And enable checkbox `"Always include JavaScript file to the site"`.

Usage example:
```
(function ($) {
  $(document).ready(function() {
    if ($('#block-1').length) {
      var waypoint = new Waypoint({
        element: document.getElementById('block-1'),
        handler: function(direction) {
          $('.another-block')
            .toggleClass('someclass', direction === 'down');
        }
      });
    }
  });
}(jQuery));
```


MAINTAINERS
-----------

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
